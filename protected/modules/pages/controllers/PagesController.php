<?php

/**
 * Pages controller
 * @package modules.pages
 */
class PagesController extends Controller
{

        public $templatePath = 'Default';
	/**
	 * Filter pages by category
	 */
	public function actionList()
	{
		// Remove "pages/" from beginning
		$url = substr(Yii::app()->request->getPathInfo(), 6);

		$model = PageCategory::model()
			->withFullUrl($_GET['url'])
			->find();

		if (!$model) throw new CHttpException(404, Yii::t('PagesModule.core', 'Категория не найдена.'));

		$criteria = Page::model()
			->published()
			->filterByCategory($model)
			->getDbCriteria();

		$count = Page::model()->count($criteria);

		$pagination = new CPagination($count);
		$pagination->pageSize = ($model->page_size > 0) ? $model->page_size: $model->defaultPageSize;
		$pagination->applyLimit($criteria);

        $pages = Page::model()->findAll($criteria);

        $view = $this->setDesign($model, 'list');

        if($model->template_path)
            $this->templatePath = $model->template_path;

        $this->render($this->templatePath.'/'.$view, array(
			'model'=>$model,
			'pages'=>$pages,
			'pagination'=>$pagination,
            'temp' => $this->templatePath,
		));
	}

	/**
	 * Display page by url.
	 * Example url: /page/some-page-url
	 * @param string $url page url
	 */
	public function actionView($url)
	{
		$model = Page::model()
			->published()
			->withUrl($url)
			->find(array(
				'limit'=>1
			));

        if (!$model) throw new CHttpException(404, Yii::t('PagesModule.core', 'Страница не найдена.'));

        if($model->category_id)
        {
            $category = PageCategory::model()->findByPk($model->category_id);
            if($category)
            {
                if($category->template_path)
                    $this->templatePath = $category->template_path;
            }
        }


		$view = $this->setDesign($model, 'view');

		$this->render($this->templatePath.'/'.$view, array(
			'model'=>$model,
            'temp' => $this->templatePath,
		));
	}

}