<?php

Yii::import('application.modules.pages.models.*');
/**
 * Display product view page.
 */
class RestPageController extends RestController
{

    public $date;

    public $pageSize;

    public $pageNumber;

    public $imageSize;

    public function beforeAction($action)
    {
        $this->pageSize = Yii::app()->request->getParam('pageSize', 10);
        $this->pageNumber = Yii::app()->request->getParam('pageNumber', 1);
        $this->imageSize = Yii::app()->request->getParam('imageSize', '500x500');

        return parent::beforeAction($action);
    }

    public function actionAuth()
    {
        $result = array(
            'success' => true,
        );
        if($error = $this->checkAuth())
        {
            $result = array(
                'error' => $error
            );
        }

        $this->_sendResponse(200, CJSON::encode($result),$this->format);

    }

    public function actionIndex()
    {
//        $this->authOrSendError();

        $this->date = Yii::app()->request->getParam('date', false);

        if(!$this->date)
            $this->_sendResponse(500, 'Error: Parameter <b>date</b> is missing' );

        $criteria = new CDbCriteria();
        $criteria->addCondition('publish_date >= \''.$this->date.'\'' );

        $model = Page::model()->findAll($criteria);

        $criteria->limit = $this->pageSize;
        $criteria->offset = ($this->pageNumber-1)*$this->pageSize;

        $count = count($model);
        $result['notesCount'] = $count;
        $result['pagesCount'] = ceil($count/$this->pageSize);
        $result['currentPage'] = $this->pageNumber;

        if($model)
        {
            $i = 0;
            foreach($model AS $page)
            {
                $result['notes'][$i]['ID'] = $page->id;
                $result['notes'][$i]['NoteName'] = $page->title;
                $result['notes'][$i]['NoteText'] = $page->full_description;
                $result['notes'][$i]['NoteDate'] = $page->publish_date;
                $result['notes'][$i]['NoteDescription'] = $page->short_description;

                if($page->mainImage){
                    $result['notes'][$i]['NoteImage'] = Yii::app()->getBaseUrl(true).$page->mainImage->getUrl($this->imageSize);
                }

                $i++;
            }
            $this->_sendResponse(200, CJSON::encode($result),$this->format);
        }
        else
            $this->_sendResponse(500, 'Error: Parameter <b>date</b> is incorrect or pages not found yet' );


    }



    public function authOrSendError()
    {
        $error = $this->checkAuth();

        if($error)
            $this->_sendResponse(500, CJSON::encode(array('errors' => $error)) ,$this->format);
    }

}