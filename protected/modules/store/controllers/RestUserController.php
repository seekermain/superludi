<?php
Yii::import('application.modules.users.UsersModule');
Yii::import('application.modules.users.forms.RemindPasswordForm');
Yii::import('application.modules.users.forms.ChangePasswordForm');
Yii::import('application.modules.orders.models.*');
/**
 * Display product view page.
 */
class RestUserController extends RestController
{

    /**
     * @var StoreProduct
     */
    public $model;

    public $firstName;

    public $lastName;

    public $fbID;

    public $UDID;

    public $date;

    public $password;

    public $email;

    public $userId;

    public $login;

    public function actionAuth()
    {
        $result = array(
            'success' => true,
        );
        if($error = $this->checkAuth())
        {
            $result = array(
                'error' => $error
            );
        }

        $this->_sendResponse(200, CJSON::encode($result),$this->format);

    }

    public function actionRegister()
    {
        $this->login = Yii::app()->request->getPost('login', false);

        if(!$this->login)
            $this->_sendResponse(500, 'Error: Parameter <b>login</b> is missing' );

        $this->firstName = Yii::app()->request->getPost('FirstName', false);

        if(!$this->firstName)
            $this->_sendResponse(500, 'Error: Parameter <b>FirstName</b> is missing' );

        $this->lastName = Yii::app()->request->getPost('LastName', false);

        if(!$this->lastName)
            $this->_sendResponse(500, 'Error: Parameter <b>LastName</b> is missing' );

        $this->email = Yii::app()->request->getPost('email', false);

        if(!$this->email)
            $this->_sendResponse(500, 'Error: Parameter <b>email</b> is missing' );

        $this->password = Yii::app()->request->getPost('password', false);

        if(!$this->password)
            $this->_sendResponse(500, 'Error: Parameter <b>password</b> is missing' );

        $this->UDID = Yii::app()->request->getPost('UDID', false);

        if(!$this->UDID)
            $this->_sendResponse(500, 'Error: Parameter <b>UDID</b> is missing' );

        $this->fbID = Yii::app()->request->getPost('Fb_ID', '');

        $this->date = Yii::app()->request->getPost('date', false);

        if(!$this->date)
            $this->_sendResponse(500, 'Error: Parameter <b>date</b> is missing' );

        $user = new User('register');
        $profile = new UserProfile;

        $user->attributes = array(
            'username' => $this->login,
            'password' => $this->password,
            'email' => $this->email,
            'ud_id' => $this->UDID,
            'fb_id' => $this->fbID,
            'created_at' => $this->date,

        );

        $profile->attributes = array(
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
        );

        $valid = $user->validate();
        $valid = $profile->validate() && $valid;

        if($valid)
        {
            $user->save();
            $profile->save();
            $profile->setUser($user);

            Yii::app()->authManager->assign('Authenticated', $user->id);

            Yii::app()->session['confirm_user_id'] = $user->id;
            Yii::app()->session['confirm_pass'] = $this->password;

            $this->_sendResponse(200, CJSON::encode(array('success' => true)),$this->format);
        }
        else
            $this->_sendResponse(500, CJSON::encode(array('UserError' => $user->getErrors(), 'ProfileError' => $profile->getErrors())),$this->format);

    }

//    public function actionDelete()
//    {
//        $this->userId = Yii::app()->request->getParam('userId', false);
//
//        if(!$this->userId)
//            $this->_sendResponse(500, 'Error: Parameter <b>userId</b> is missing' );
//
//        $models = User::model()->findAllByPk($this->userId);
//
//        if(!empty($models))
//        {
//            foreach ($models as $user)
//            {
//                if ($user && ($user->id != Yii::app()->user->id))
//                    $user->delete();
//            }
//            $this->_sendResponse(200, CJSON::encode(array('success' => true)),$this->format);
//        }
//        else
//            $this->_sendResponse(500, 'Error: Parameter <b>userId</b> is incorrect' );
//
//    }
//
//    public function actionSelect()
//    {
//        $this->userId = Yii::app()->request->getParam('userId', false);
//
//        if($this->userId)
//            $model = User::model()->findAllByPk($this->userId);
//        else
//            $model = User::model()->findAll();
//
//        if($model)
//        {
//            $i = 0;
//            foreach($model AS $user)
//            {
//                $profile = $user->profile;
//                $result[$i]['id'] = $user->id;
//                $result[$i]['username'] = $user->username;
//                $result[$i]['email'] = $user->email;
//                $result[$i]['ud_id'] = $user->ud_id;
//                $result[$i]['fb_id'] = $user->fb_id;
//                $result[$i]['created_at'] = $user->created_at;
//                $result[$i]['firstName'] = $profile->firstName;
//                $result[$i]['lastName'] = $profile->lastName;
//                $i++;
//            }
//            $this->_sendResponse(200, CJSON::encode($result),$this->format);
//        }
//        else
//            $this->_sendResponse(500, 'Error: Parameter <b>userId</b> is incorrect or users not found yet' );
//    }

    public function getFullNames()
    {
        return array(
            'id' => 'user_id',
            'name' => 'full_name',
            'phone' => 'phone',
            'email' => 'email',
        );
    }

    public function authOrSendError()
    {
        $error = $this->checkAuth();

        if($error)
            $this->_sendResponse(500, CJSON::encode(array('errors' => $error)) ,$this->format);
    }

}